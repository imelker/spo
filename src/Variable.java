
public class Variable {
	public String name;
	public double value;
	
	public Variable(String inputName, String inputValue) {
		name = inputName;
		value = Double.parseDouble(inputValue);
	}
}
