import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Stack;

public class Parser {
	
	ArrayList<Variable> varList = new ArrayList<Variable>();

	/* ���������� �������� ����������. ��������� �����. */
	private void setVariable(Variable inputVar) {
		for (Variable iv : varList){
			if (iv.name.equals(inputVar.name)) {
				iv.value = inputVar.value;
				return;
			}
		}
		varList.add(inputVar);
		return;
	}
	
	private Double getVariable(String varName) {
		for (Variable iv : varList){
			if (iv.name.equals(varName))
				return iv.value;
		}
		return null;
	}


/* ��������� �������������� ��������. ��������� �����. */
	private Token doMath(String operation,Token intToken_1,Token intToken_2) {
		Token outToken = new Token("int","0",0);
		
		switch(operation) {
		case "dot":
			outToken.input = Double.toString(Double.parseDouble(intToken_1.input) + Double.parseDouble(intToken_2.input)/10);
			break;
		case "mul":
			outToken.input = Double.toString(Double.parseDouble(intToken_1.input) * Double.parseDouble(intToken_2.input));
			break;
		case "div":
			if (intToken_2.input.equals("0")) {
				outToken.name = "error";
				outToken.input = "������� �� 0";
				return outToken;
			}
			outToken.input = Double.toString(Double.parseDouble(intToken_1.input) / Double.parseDouble(intToken_2.input));
			break;
		case "add":
			outToken.input = Double.toString(Double.parseDouble(intToken_1.input) + Double.parseDouble(intToken_2.input));
			break; 
		case "sub":
			if (intToken_1 == null)
				outToken.input = Double.toString(0 - Double.parseDouble(intToken_2.input));
			else
				outToken.input = Double.toString(Double.parseDouble(intToken_1.input) - Double.parseDouble(intToken_2.input));
			break;
		case "pow":
			outToken.input = Double.toString(Math.pow(Double.parseDouble(intToken_1.input), Double.parseDouble(intToken_2.input)));
			break;
		default:
			outToken.name = "error";
			outToken.input = "����������� ��������";
			return outToken;
		}
		return outToken;
	}
	
/* ��������� ������� */
	public String parse(LinkedList<Token> tokenList) {
		
		//����� ������
		if (tokenList.getFirst().name.equals("help")) 
		{
			if (tokenList.size() != 1)
				return "Syntax error 02 after 'help'";
			
			return "������ �����: [����������] = [���������].\n\n" +
					"��������� ����� ���������:\n" +
					"+ - ��������\n" +
					"- - ���������\n" +
					"/ - �������\n" +
					"* - ���������\n" +
					"^ - ���������� � �������\n" +
					"() - brackets for prioritizing.\n\n" + 
					"clear - ������� ��� ����������";
		}
		
		
		
		//�������� ���� ���������� �� ������
		if (tokenList.getFirst().name.equals("clear")) 
		{
			if (tokenList.size() != 1)
				return "Syntax error 03 after 'clear'";
			varList.clear();
				return "��� ���������� ���� �������";
		}
		
		//����� �������� ���������� ���� ��������� ����� (echo)
		if (tokenList.getFirst().name.equals("echo")) {
			if (tokenList.size() != 2)
				return "Syntax error 04. ������� ����� �������� �������� ������ ����� ����������.";
			
			if (tokenList.get(1).name.equals("var")) {
				Double echoVar = getVariable(tokenList.get(1).input);
				if (echoVar != null)
					return Double.toString(echoVar);
				else
					return "Error 05. ��� ����� ���������� " + tokenList.get(1).input + ".";
			}
			
			if (tokenList.get(1).name.equals("int"))
				return tokenList.get(1).input;
			
			return "Syntax error 06. ������� echo ����� ���������� ������ �����.";
		}
		
		
		//���������������� ���� ��������
		if (tokenList.size() < 3)
			return "Syntax error 07. ������������ ����������� �� ��������������";
		
		//������������
		if (tokenList.getFirst().name.equals("var") && tokenList.get(1).name.equals("assign"))
			{
			Token tempToken = tokenList.get(0);
			tokenList.remove(0);
			tokenList.remove(0);
			
			for (Token tv : tokenList){
				Double intValue;
				if (tv.name.equals("var")) {
					intValue = getVariable(tv.input);
					if (intValue == null)
						return null;
					else {
						tv.name = "int";
						tv.input = Double.toString(intValue);
						tv.lvl = 0;
					}
				}
			}
			
			if (!tokenList.get(0).name.equals("int") && !tokenList.get(0).name.equals("sub"))
				return "Syntax error 26. ����� ��������� ������ �����.";
			else {
				Token resToken = calculator(tokenList);
				if(!resToken.name.equals("error"))
				{
					Variable varToSet = new Variable(tempToken.input,resToken.input);
					setVariable(varToSet);
					return (varToSet.name + " = " + varToSet.value);
				}
				else
				{
					return "Syntax error 09. " + resToken.input;
				}
				
			}
		}
		else
			return "Syntax error 08. ����������� '" + tokenList.getFirst().name + " " + tokenList.get(1).name + "' �� ��������������.";
	}
	

	 private Token calculator(LinkedList<Token> tokenList) {
		 String systemOut = "";
		 for(Token tv: tokenList)
			 systemOut += tv.input;
		 System.out.println("�����������:" + systemOut);
		 
	     //�������� �� ��������
	     for(int i =0; i < tokenList.size();i++) {
	         if(tokenList.get(i).name.equals("lp")) {
	             LinkedList<Token> curTokenList = new LinkedList<Token>();
	             tokenList.remove(i);
	             int bracketsCount=0;
	             for(int j = i;j < tokenList.size();j++) {
	                 curTokenList.add(tokenList.get(j));
	                 if(tokenList.get(j).name.equals("lp"))
	                     bracketsCount++;
	                 if(tokenList.get(j).name.equals("rp") && bracketsCount!=0)
	                     bracketsCount--;
	                 tokenList.remove(j);
	                 if(tokenList.get(j).name.equals("rp") && bracketsCount==0) {
	                     tokenList.remove(j);
	                     break;
	                 }
	                 j--;
	             }
	             tokenList.add(i,calculator(curTokenList));
	             i--;
	         }
	     }
	     
	     if(tokenList.get(0).input.equals("-")) //���� �������� ���������� � ������
	     {
             tokenList.set(1,doMath(tokenList.get(0).name,null,tokenList.get(1)));
             tokenList.remove(0);
	     }
	     
	     for(int i =0; i < tokenList.size();i++) //������� ����� � ����� � ��������� �������
	     {
	         if(tokenList.get(i).lvl==5) {
	             tokenList.set(i-1,doMath(tokenList.get(i).name,tokenList.get(i-1),tokenList.get(i+1)));
	             tokenList.remove(i);
	             tokenList.remove(i);
	             i-=2;
	         }
	     }

	     for(int i =0; i < tokenList.size();i++) //��������� � �������
	     {
	         if(tokenList.get(i).lvl==2) {
	             tokenList.set(i-1,doMath(tokenList.get(i).name,tokenList.get(i-1),tokenList.get(i+1)));
	             tokenList.remove(i);
	             tokenList.remove(i);
	             systemOut = "";
	    		 for(Token tv: tokenList)
	    			 systemOut += tv.input;
	    		 System.out.println("����� ���������/�������: " + systemOut);
	             i-=2;
	         }
	     }

	     for(int i =0; i < tokenList.size();i++) //�������� � �����
	     {
	         if (tokenList.get(i).lvl == 1) {
	             Token tempToken = doMath(tokenList.get(i).name, tokenList.get(i - 1), tokenList.get(i + 1));
	             tokenList.set(i - 1, tempToken);
	             tokenList.remove(i);
	             tokenList.remove(i);
	             systemOut = "";
	    		 for(Token tv: tokenList)
	    			 systemOut += tv.input;
	    		 System.out.println("����� �����/��������: " + systemOut);
	             i-=2;
	         }
	     }
	     return tokenList.get(0);
	 }
}
