import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lexer {
	
	ArrayList<Token> maskList;
	LinkedList<Token> tokenList;
	Token curToken;
	
	Lexer() {
		tokenList = new LinkedList<Token>(); //������ �������
		maskList = new ArrayList<Token>(); //������������ ������
		
		maskList.add(new Token("help","help",4));
		maskList.add(new Token("echo","echo",4));
		maskList.add(new Token("clear","clear",4));
		maskList.add(new Token("var","[a-z]{1,}[A-Za-z]{0,}",4));
		maskList.add(new Token("int","0",0));
		maskList.add(new Token("int","[1-9]{1}[0-9]{0,9}",0));
		maskList.add(new Token("assign","[=]{1}",4));
		//���������
		maskList.add(new Token("add","[+]{1}",1));
		maskList.add(new Token("sub","[-]{1}",1));
		maskList.add(new Token("mul","[*]{1}",2));
		maskList.add(new Token("div","[/]{1}",2));
		maskList.add(new Token("dot","[.]{1}",5));
		maskList.add(new Token("pow","[\\^]{1}",2));
		//��������
		maskList.add(new Token("lp","[(]{1}",3));
		maskList.add(new Token("rp","[)]{1}",3));
		//������
		maskList.add(new Token("error","[A-Za-z]{1,}[0-9]{1,}",4));
		maskList.add(new Token("error","[0]{1,}[0-9]{1,}",4));
		maskList.add(new Token("error","[0-9]{11,}",4));
		maskList.add(new Token("error","[.]{1}[0-9]{0,9}[.]{1}",4));
		maskList.add(new Token("error","[+,\\-,/,*,.,\\^]{1}[+,\\-,/,*,\\^,.]{1,}",4));
	}
	
	private boolean matchToken(String tokenString) {
		for (Token tm : maskList)
		{
			Pattern tokenPattern = Pattern.compile(tm.input);
			Matcher tokenMatcher = tokenPattern.matcher(tokenString);

			if (tokenMatcher.matches()) 
			{
				if (tm.name.equals("error")) 
				{
					curToken = null;
					return false;
				}
				curToken = new Token(tm.name, tokenString,tm.lvl); 
				return true;
			}
		}
		return false;
	}
	
	/* ������ � ������ */
	public LinkedList<Token> read(String inputString) {
		tokenList.clear();
		int length = 1;
		curToken = null;
		
		for(int pos = 1; pos <= inputString.length(); pos++) 
		{
			String tokenString = inputString.substring(pos - length, pos);
			
			if (length == 1 && tokenString.equals(" "))
				continue;
			
			if (matchToken(tokenString)) 
			{
				if (pos == inputString.length() && curToken != null)
					tokenList.add(curToken);
				length++;
			}
			else if (curToken != null) 
			{
				tokenList.add(curToken);
				length = 1;
				curToken = null;
				pos--;
			}
			else 
			{
				System.out.println("\nSyntax error 01 at " + pos + ". ����������� ����� " + inputString.substring(pos-length,pos) + ".");
				return null;
			}
		}
		return tokenList;
	}
}
